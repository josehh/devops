<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'passwd' );

/** MySQL hostname */
define( 'DB_HOST', 'database-1.cluster-ccagf5we0ktw.us-east-2.rds.amazonaws.com' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '<kYsQ>])w_seKf4Y?S9IU<LgLC8.ysuvn{]b_.TvacJ-0q*3w=FE TOwcqB}?bK@' );
define( 'SECURE_AUTH_KEY',  '3m?]04gV,%u`%^,y,*Ad!U>@Y^.)U+_0-x%L$Rt]~ZK&d#b=c5 dAj`f-0nC]*;t' );
define( 'LOGGED_IN_KEY',    '% QEHF4tJC#]n2<_DG^+{(o.FEcq]}<>*V~p:>eAtq&Aa;P)(zY;<IxZg9Z.;3;h' );
define( 'NONCE_KEY',        'w&sHqQRPw%|d2l-m).vd^LFpMr~8L7H>vq~dCO=57JfN5r14?YowJYw J.OL2]7A' );
define( 'AUTH_SALT',        '>gk;&e),DC`yF+RfpA;.c0/J}~+G Vji1Z0.:kEz9g-u&akc-sO?(|%2~[&o.q)~' );
define( 'SECURE_AUTH_SALT', '_&BCz2Md6]y0L+cy=&$)`c7:!@-DKlya*U_eH}]#?<d2!m:{#Doo&jV[d2NPnCi(' );
define( 'LOGGED_IN_SALT',   '_Y&$DCG+66.iD4Z+!n}Rx|FnQemrmBrP~:+WZxs_dhzv+U<v^2$%K^f=;.) .%*W' );
define( 'NONCE_SALT',       'Hl)h)T8E(iP|g#OxHf/Eq%NjNfX|##-I>T~Qr^qrFqYrKReyTzXUIp-h1p,[aM1i' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
